package crypto_ware.crypto_dash.tutorial_section;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.io.File;

import crypto_ware.crypto_dash.R;

public class CardInfo {
    protected String title;
    protected String subText;
    protected int icon;
    protected File htmlFile;

    public CardInfo(String title, String subText, int icon, File htmlFile) {
        this.title = title;
        this.subText = subText;
        this.icon = icon;
        this.htmlFile = htmlFile;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubText() {
        return subText;
    }

    public void setSubText(String subText) {
        this.subText = subText;
    }

    public void setHtmlFile(File htmlFil) { this.htmlFile = htmlFile; }

    public File getHtmlFile() { return htmlFile; }

}
