package crypto_ware.crypto_dash.tutorial_section;

import android.content.res.AssetManager;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.net.URI;
import java.util.List;

import crypto_ware.crypto_dash.R;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardViewHolder> {

    private List<CardInfo> cardList;
    private WebView webview;

    public CardAdapter(List<CardInfo> cardList) {
        this.cardList = cardList;
    }


    @Override
    public int getItemCount() {
        return cardList.size();
    }

    @Override
    public void onBindViewHolder(CardViewHolder cardViewHolder, int i) {
        CardInfo ci = cardList.get(i);
        cardViewHolder.vTitle.setText(ci.getTitle());
        cardViewHolder.vSubText.setText(ci.getSubText());
        cardViewHolder.vImage.setImageResource(ci.getIcon());


        //Set the item click listener
        cardViewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if(cardList.get(pos).getHtmlFile() != null) {
                    Tutorials_Section.getWV().loadUrl(cardList.get(pos).getHtmlFile().toString());
                    Tutorials_Section.getWV().setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_layout, viewGroup, false);

        return new CardViewHolder(itemView);
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView vImage;
        protected TextView vTitle;
        protected TextView vSubText;
        ItemClickListener itemClickListener;

        public CardViewHolder(View v) {
            super(v);

            vTitle =  (TextView) v.findViewById(R.id.title);
            vSubText = (TextView) v.findViewById(R.id.subText);
            vImage = (ImageView) v.findViewById(R.id.url);

            itemView.setOnClickListener(this);
        }

        //When clicked
        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }


        //Will be called outside of this class
        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }
}