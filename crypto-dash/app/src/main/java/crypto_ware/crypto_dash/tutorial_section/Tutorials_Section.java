package crypto_ware.crypto_dash.tutorial_section;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import crypto_ware.crypto_dash.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tutorials_Section.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tutorials_Section#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tutorials_Section extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private RecyclerView cardList;
    private CardView cardView;
    private static WebView webview;

    private LinearLayoutManager layoutManager;

    private TextView info_text;

    public Tutorials_Section() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tutorials_Section.
     */
    // TODO: Rename and change types and number of parameters
    public static Tutorials_Section newInstance(String param1, String param2) {
        Tutorials_Section fragment = new Tutorials_Section();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rView = inflater.inflate(R.layout.fragment_tutorials__section, container, false);
        //cardList = (RecyclerView) rView.findViewById(R.id.cardList);

        super.onCreate(savedInstanceState);

        RecyclerView recList = (RecyclerView) rView.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        CardAdapter ca = new CardAdapter(createList());
        recList.setAdapter(ca);

        webview = (WebView) rView.findViewById(R.id.webview);
        webview.loadUrl("http://tompedraza.com");
        webview.setVisibility(View.GONE);


        return rView;
    }


    public static WebView getWV() {
        return webview;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public static void OnBackPressed(){
        if(webview.getVisibility() == View.VISIBLE)
        webview.setVisibility(View.GONE);
    }

    private List<CardInfo> createList() {

        List<CardInfo> result = new ArrayList<>();

        result.add(new CardInfo("\"What are crypto-currencies?\"",
                "Tap here to read",
                R.drawable.bitcoin, new File("File:///android_asset/whatIsCrypto.html")));
        result.add(new CardInfo("Graphs and Candlesticks",
                "Learn about the tools that Crypto-dash has to offer",
                R.drawable.bitcoin,
                new File("File:///android_asset/graphTutorial")));
        result.add(new CardInfo("When to invest",
                "Tips on when you should invest money in the market",
                R.drawable.bitcoin,
                new File("File:///android_asset/whenToInvest")));
        result.add(new CardInfo("How much to invest",
                "Benchmarks for how much you should invest given your available amount",
                R.drawable.bitcoin,
                new File("File:///android_asset/hmtoInvest")));
        return result;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
