package crypto_ware.crypto_dash.tutorial_section;

import android.view.View;

/**
 * Created by Lunar on 3/10/2018.
 */

public interface ItemClickListener {
    void onItemClick(View v, int pos);
}
