package crypto_ware.crypto_dash.main;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.rd.PageIndicatorView;

import crypto_ware.crypto_dash.R;
import crypto_ware.crypto_dash.dashboard.Dashboard;
import crypto_ware.crypto_dash.news_feed.News_Feed;
import crypto_ware.crypto_dash.tutorial_section.Tutorials_Section;

public class MainActivity extends FragmentActivity implements News_Feed.OnFragmentInteractionListener, Dashboard.OnFragmentInteractionListener, Tutorials_Section.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setting up the View Pager
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        FragmentManager fragmentManager = getSupportFragmentManager();
        PagerAdapter pagerAdapter = new PagerAdapter(fragmentManager);

        // This is where the control of which fragment appears first occurs.
        viewPager.setAdapter(pagerAdapter);
        // Displays the Dashboard first.
        viewPager.setCurrentItem(pagerAdapter.DASHBOARD);

        final PageIndicatorView pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(pagerAdapter.getCount());
        pageIndicatorView.setSelection(pagerAdapter.DASHBOARD);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageIndicatorView.setSelection(position);
                if (position == 0) {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    //mass comment CTRl+/, undo CTRL+SHIFT+/
}
