package crypto_ware.crypto_dash.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import crypto_ware.crypto_dash.dashboard.Dashboard;
import crypto_ware.crypto_dash.news_feed.News_Feed;
import crypto_ware.crypto_dash.tutorial_section.Tutorials_Section;

/**
 * Created by Logan Stanfield on 1/30/18.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    // These public values will be able to be referenced in the main activity as necessary.
    public final int NEWS_FEED = 0;
    public final int DASHBOARD = 1;
    public final int TUTORIALS_SECTION = 2;
    private final int NUM_TABS = 3;


    /**
     * PagerAdapter constructor.
     *
     * @param fragmentManager - Takes in a FragmentManager object in which is passed to the super().
     */
    public PagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    /**
     *
     * @param position - This method will return the fragment based on the position.
     *                 0 - Will be the News Feed fragment.
     *                 1- Will be the Dashboard fragment (This one will be displayed first).
     *                 2 - Will be the Tutorials Section.
     * @return The Fragment object.
     */
    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case NEWS_FEED:
                return new News_Feed();
            case DASHBOARD:
                return new Dashboard();
            case TUTORIALS_SECTION:
                return new Tutorials_Section();
            default:
                return null;
        }
    }

    /**
     * This method returns the total number of tabs/fragments.
     *
     * @return - The total number of tabs.
     */
    @Override
    public int getCount() {
        return NUM_TABS;
    }
}
