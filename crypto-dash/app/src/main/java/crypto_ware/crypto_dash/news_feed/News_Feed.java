package crypto_ware.crypto_dash.news_feed;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import crypto_ware.crypto_dash.R;
import crypto_ware.crypto_dash.utility.network_utility.NetworkStateReceiver;


public class News_Feed extends Fragment implements NetworkStateReceiver.NetworkStateReceiverListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    // URL string that is obtained by the NewsAPI.
    private String topHeadlinesAddress = "https://newsapi.org/v2/top-headlines?q=cryptocurrency&apiKey=f62822559f9045869f4075a346f3fe88";

    // Thread and task related variables.
    private volatile RequestTask requestTask = new RequestTask();
    private volatile RequestNews requestNews;
    private Handler handler;

    // Network handling variables.
    private NetworkStateReceiver networkStateReceiver;

    /* This boolean checks to see if the networkAvailable() method has not been called back to back.
       For some strange reason when the network connection becomes reconnected the method is called
       several times so this is a safe guard to prevent that from happening.
     */
    private boolean networkAvailabilityAlreadyCalled = false;

    // CHANGE THE INTERVAL VARIABLE HERE -> time is in milliseconds. Default = 3600000
    private long newsRequestInterval = 60 * 60 * 1000;

    public News_Feed() {
        // Required empty public constructor
    }

    public static News_Feed newInstance(String param1, String param2) {
        News_Feed fragment = new News_Feed();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        // Sets up the network receiver listener objects.
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        getActivity().registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        handler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment]

        View view = inflater.inflate(R.layout.fragment_news_feed, container, false);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        networkStateReceiver.removeListener(this);
        this.getActivity().unregisterReceiver(networkStateReceiver);
        this.stopRequestTask();
    }

    @Override
    public void networkAvailable() {
        Log.i("NETWORK", "AVAILABLE NEWS FEED");
        if (!networkAvailabilityAlreadyCalled) {
            networkAvailabilityAlreadyCalled = true;
            this.startRequestTask();
        }
    }

    @Override
    public void networkUnavailable() {
        Log.i("NETWORK", "UNAVAILABLE NEWS FEED");
        networkAvailabilityAlreadyCalled = false;
        this.stopRequestTask();
        Toast.makeText(getContext(), "Please connect to the internet", Toast.LENGTH_SHORT).show();
    }

    //Use this to interact with other fragments and their activities.
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * This method starts the handler with a delay of 100ms.
     */
    public void startRequestTask() {
        handler.postDelayed(requestTask, 100);
    }

    /**
     * This method removes the request task from the handler callbacks. Thus stopping the interval.
     */
    public void stopRequestTask() {
        handler.removeCallbacks(requestTask);
    }

    /**
     * This private class implements a Runnable in which is used by the handler to be called at an
     * interval. This is used to request the news for the news feed.
     */
    private class RequestTask implements Runnable {

        public RequestTask() {
            // Empty constructor.
        }

        @Override
        public void run() {
            Log.i("NEWS FEED", "UPDATING NEWS.....");
            requestNews = (RequestNews) new RequestNews().execute(topHeadlinesAddress);
            Log.i("NEWS FEED", "NEWS UPDATED!");
            handler.postDelayed(this, newsRequestInterval);
        }
    }

    private class RequestNews extends AsyncTask<String, Void, String> {
        protected NewsAdapter adapter;
        protected RecyclerView recList;
        public static final int CONNECTION_TIMEOUT = 15000;
        public static final int READ_TIMEOUT = 15000;

        URL url = null;
        String response = "";
        String inputLine = "";
//        ProgressDialog progress = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progress.setMessage("Loading articles...");
//            progress.setCancelable(true);
//            progress.show();
        }

        @Override
        protected String doInBackground(String... address) {
            //thread name : "AsyncTask #1"
            try {
                URL url = new URL(address[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setRequestMethod("GET");

                BufferedReader buff = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                StringBuilder sb = new StringBuilder();
                while ((inputLine = buff.readLine()) != null) {
                    sb.append(inputLine);
                }
                response = sb.toString();
                buff.close();
                connection.disconnect();
                return response;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // progress.dismiss();
            //Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
            List<News> articleList = new ArrayList<>();
            int total;
            try {
                JSONObject jsonObject = new JSONObject(result);
                total = Integer.valueOf(jsonObject.getString("totalResults"));

                JSONArray jsonArray = jsonObject.getJSONArray("articles");
                jsonArray = jsonArray;

                String length = String.valueOf(jsonArray.length());

                String id, name, author, title, description, address, urlToImage, publishedAt = "";

                for (int i = 0; i < total; i++) {
                    id = jsonArray.optJSONObject(i).optJSONObject("source").getString("id");
                    name = jsonArray.optJSONObject(i).optJSONObject("source").getString("name");
                    author = jsonArray.optJSONObject(i).getString("author");
                    title = jsonArray.optJSONObject(i).getString("title");
                    description = jsonArray.optJSONObject(i).getString("description");
                    address = jsonArray.optJSONObject(i).getString("url");
                    urlToImage = jsonArray.optJSONObject(i).getString("urlToImage");
                    publishedAt = jsonArray.optJSONObject(i).getString("publishedAt");

                    articleList.add(new News(id, name, author, title, description, address, urlToImage, publishedAt));
                }
                adapter = new NewsAdapter(getActivity(), articleList);
                recList = (RecyclerView) (getView().findViewById(R.id.cardList));
                recList.setAdapter(adapter);
                recList.setLayoutManager(new LinearLayoutManager(getActivity()));

                this.cancel(true);
                if (this.isCancelled()) {
                    // progress.dismiss();
                    // Toast.makeText(getBaseContext(), String.valueOf(length) + " Results Fetched.", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e3) {
                e3.printStackTrace();
            } catch (Exception ex) {
                Log.i("CAUGHT", "POST EXECUTE NEWS FEED UNKNOWN");
            }
        }
    }
}
