package crypto_ware.crypto_dash.news_feed;

public class News {

    protected String id = "";
    protected String name = "";
    protected String author = "";
    protected String title = "";
    protected String description = "";
    protected String url = "";
    protected String urlToImage = "";
    protected String publishedAt = "";

    public News(String i, String na, String au, String t, String de, String ur, String ui, String pa){
        this.id = i;
        this.name = na;
        this.author = au;
        this.title = t;
        this.description = de;
        this.url = ur;
        this.urlToImage = ui;
        this.publishedAt = pa;
    }

    public void clearAll(){
        this.id = "";
        this.name = "";
        this.author = "";
        this.title = "";
        this.description = "";
        this.url = "";
        this.urlToImage = "";
        this.publishedAt = "";
    }
//call News getters here
}