package crypto_ware.crypto_dash.news_feed;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;

import crypto_ware.crypto_dash.R;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    //    private List<News> list;
    private Context context;
    private LayoutInflater inflater;
    private List<News> list = Collections.emptyList();
    private News current;
    private NewsViewHolder newsViewHolder;
    int currentPos = 0;

    //Constructor to initialize context & data from MainActivity.
    public NewsAdapter(Context context, List<News> list) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.list = list;
    }

    //Inflates the relative layout / cardview xml FILE, NOT ID, into view hierarchy.
    //Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent
    //an item. int viewType is the type of item at 'viewType' position.
    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(viewGroup.getContext()).
//                inflate(R.layout.item_news, viewGroup, false);
        View view = inflater.inflate(R.layout.item_news, parent, false);
        NewsViewHolder holder = new NewsViewHolder(view);
        return holder;
//        return new ContactViewHolder(itemView);
    }

    //bind data to current pos in recycler view, assign vals from article list.
    @Override
    public void onBindViewHolder(NewsViewHolder holder, int iType) {
        newsViewHolder = (NewsViewHolder) holder;
        this.current = list.get(iType);
        newsViewHolder.name.setText(current.name);
        newsViewHolder.author.setText(current.author);
        newsViewHolder.title.setText(current.title);
        newsViewHolder.description.setText(StringUtils.abbreviate(current.description, 150));
        newsViewHolder.url.setText(current.url);
        newsViewHolder.publishedAt.setText(current.publishedAt);
        Glide.with(context).load(current.urlToImage).override(1000, 300)
                .into(newsViewHolder.img);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recView) {
        super.onAttachedToRecyclerView(recView);
    }

    //inherits from RecyclerView methods / members
    class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name;
        TextView author;
        TextView title;
        TextView description;
        TextView url;
        TextView publishedAt;
        ImageView img;
        CardView card_view;
        RelativeLayout layout;

        //constructor to construct TextView / ImageView widget reference.
        //call super() for the view from RecyclerView.ViewHolder?
        //View view is the item view.
        public NewsViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            author = (TextView) view.findViewById(R.id.author);
            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            url = (TextView) view.findViewById(R.id.address);
            publishedAt = (TextView) view.findViewById(R.id.publishedAt);
            img = (ImageView) view.findViewById(R.id.img);
            card_view = (CardView) view.findViewById(R.id.card_view);
            layout = (RelativeLayout) view.findViewById(R.id.layout);

            // TODO: Figure out why it bugs out with card view. Card view on click doesn't work.
            layout.setOnClickListener(this);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d("Taking you to ->", url.getText().toString());
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url.getText().toString()));
            context.startActivity(browserIntent);
        }
    }
}