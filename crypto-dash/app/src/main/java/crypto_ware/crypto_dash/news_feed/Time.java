package crypto_ware.crypto_dash.news_feed;

import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;

public class Time {
    int chigum = 0;
    ArrayList<String> list = null;

    public Time() {
        chigum = 0;
        this.list = new ArrayList<>();
    }

    public List<String> getDateList(){
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        Integer year = 0;
        String yr = "";
        Integer day = 0;
        String d = "";
        String m = "";
        Integer month = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        String str = dateFormat.format(cal.getTime());

        System.out.println("Date: " + str);

        month = cal.get(Calendar.MONTH);
        month = month + 1;
        year = cal.get(Calendar.YEAR);
        day = cal.get(Calendar.DAY_OF_MONTH);

        yr = String.valueOf(year);
        m = String.valueOf(month);
        d = String.valueOf(day);

        System.out.println("Adjusted Month:  " + m);
        System.out.println("Day: " + d);
        System.out.println("Year: " + yr);
        list.add(yr);
        list.add(m);
        list.add(d);

        return list;
    }
}
