package crypto_ware.crypto_dash.dashboard.coins;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * OHLCV Object represents all data points necessary for one "Candle" in a candle stick chart.a
 * An array of these objects will be used to plot charts in the Dashboard system.
 * Created by max on 2/8/18.
 */

public class OHLCV {

    private Date time;
    private double open, high, low, close, volumeFrom, volumeTo;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");


    /**
     * Blank Constructor
     */
    public OHLCV () {

    }

    /**
     * Full Constructor
     * @param t
     * @param o
     * @param h
     * @param l
     * @param c
     * @param vFrom
     * @param vTo
     */
    public OHLCV (Date t, double o, double h, double l, double c, double vFrom, double vTo){
        this.time = t;
        this.open = o;
        this.high = h;
        this.low = l;
        this.close = c;
        this.volumeFrom = vFrom;
        this.volumeTo = vTo;
    }

    //Setter methods
    public void setTime(Date t) { this.time = t; }

    public void setOpen(double o) {
        this.open = o;
    }

    public void setHigh(double h) {
        this.high = h;
    }

    public void setLow(double l) {
        this.low = l;
    }

    public void setClose(double c) {
        this.close = c;
    }

    public void setVolFrom(double vFrom) {
        this.volumeFrom = vFrom;
    }

    public void setVolTo(double vTo) {
        this.volumeTo = vTo;
    }

    //Getter methods
    public Date getTime() {
        return time;
    }

    public double getOpen() {
        return open;
    }

    public double getHigh() {
        return high;
    }

    public double getLow() {
        return low;
    }

    public double getClose() {
        return close;
    }

    public double getVolFrom() {
        return volumeFrom;
    }

    public double getVolTo() {
        return volumeTo;
    }

    /**
     * Returns Date field stored in OHLCV Object in readable format based on machine's current time zone
     * @return, formatted date as String
     */
    public String getTimeAsString() {
        DATE_FORMAT.setTimeZone(TimeZone.getDefault());
        return DATE_FORMAT.format(this.time);
    }

    /**
     * Standard to string method
     * @return
     */
    public String toString() {

        return "OHLCV:\n\tTime: " + getTimeAsString() + "\n\t"
                       + "Open: $" + this.open + "\n\t"
                       + "High: $" + this.high + "\n\t"
                       + "Low: $" + this.low + "\n\t"
                       + "Close: $" + this.close + "\n\t"
                       + "Volume From: " + this.volumeFrom + "\n\t"
                       + "Volume To:" + this.volumeTo + "\n";
    }
}
