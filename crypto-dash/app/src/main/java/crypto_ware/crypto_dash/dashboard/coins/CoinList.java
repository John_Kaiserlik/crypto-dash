package crypto_ware.crypto_dash.dashboard.coins;

import android.os.AsyncTask;

import java.util.List;

/**
 * CoinList Object will contain the full list of coins supported by cryptocompare.com/api
 * Upon calling constructor the request will be sent out and handled through GetCoinList.
 * This class is currently still missing implementation for sorting, and binary search.
 * For now a crude search is being performed for testing purposes.
 */

public class CoinList {

    //List of all available coin objects
    private List<Coin> coinList;
    //Coin List Url
    private static String coinListUrl = "https://min-api.cryptocompare.com/data/all/coinlist";
    //Flag for wait on request completion
    private volatile boolean reqComp;

    public CoinList() {
        reqComp = false;
        new GetCoinList().execute();
        while (!reqComp) {
            //wait while request goes through
        }
    }

    /**
     * This class sends out the network request for the full coin list. The actual request and parsing
     * of request is handled by provided methods of RequestHandle class.
     * (For Logan) Corresponding GUI updates will likely be added into onPostExecute() of this class.
     */
    private class GetCoinList extends AsyncTask<Void, Void, Void> {
        //Tag for any messages related to class
        private static final String TAG = "GetCoinList";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //Initialize RequestHandle object
            RequestHandle rhCL = new RequestHandle();
            //Make a request based on given url
            String coinListRaw = rhCL.execRequest(coinListUrl);

            if (coinListRaw != null) {
                //Initialize coin list object;
                coinList = rhCL.parseCoinList(coinListRaw);
                reqComp = true;
            }
            return null;
        }

        /**
         * GUI updates go here
         * @param aVoid
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    //THIS IS A CRUDE SEARCH FOR TESTING, NEEDS TO BE UPDATED
    public Coin getCoin(String name) {
        for (int i = 0; i < coinList.size(); i ++) {
            Coin nxt = coinList.get(i);
            if (name.equalsIgnoreCase(nxt.getName())) {
                return nxt;
            }
        }
        return null;
    }

}
