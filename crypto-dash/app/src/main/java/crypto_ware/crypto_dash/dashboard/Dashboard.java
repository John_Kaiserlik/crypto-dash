package crypto_ware.crypto_dash.dashboard;

import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.xy.CatmullRomInterpolator;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Arrays;
import java.util.List;

import crypto_ware.crypto_dash.R;
import crypto_ware.crypto_dash.dashboard.coins.Coin;
import crypto_ware.crypto_dash.dashboard.coins.OHLCV;
import crypto_ware.crypto_dash.utility.network_utility.NetworkStateReceiver;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Dashboard.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Dashboard#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Dashboard extends Fragment implements NetworkStateReceiver.NetworkStateReceiverListener {
    //Boolean Flag for network request status
    public static boolean ready = false;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // Fragment listener.
    private OnFragmentInteractionListener mListener;

    // Timers and such.
    private Handler handler;

    /* This boolean checks to see if the networkAvailable() method has not been called back to back.
       For some strange reason when the network connection becomes reconnected the method is called
       several times so this is a safe guard to prevent that from happening.
     */
    private boolean networkAvailabilityAlreadyCalled = false;

    // CHANGE THE DELAY TO RETRIEVE COIN DATA HERE. -> DEFAULT: 10000ms = 10 seconds
    private long updateDashboardInterval = 10000;
    private long updatePlotInterval = 10000;

    // =========== UI Components ===========
    // TextView objects
    private TextView priceView;
    private TextView priceLblView;
    private TextView supplyView;
    private TextView changeDayView;
    private TextView openDayView;
    private TextView highDayView;
    private TextView lowDayView;
    private TextView marketCapView;
    private TextView change24View;
    private TextView open24View;
    private TextView high24View;
    private TextView low24View;

    // Button objects
    private Button btcBtn;
    private Button ethBtn;
    private Button ltcBtn;
    private Button minChartBtn;
    private Button hourChartBtn;
    private Button dayChartBtn;


    // TODO: Set up plotting.
    private XYPlot coinPlot;
    private TextView plotTitle;

    // TODO: Use a multi-state switch instead of setting up boolean flags.
    private boolean minutePressed = true;
    private boolean hourPressed = false;
    private boolean dayPressed = false;

    // Network handling variables.
    NetworkStateReceiver networkStateReceiver;

    // DEBUGGING PRINT FLAGS
    boolean debugPrintCoinPrice = false;

    // TODO: Starts the coin task using bitcoin.
    private String coinString = "BTC";
    private volatile CoinTask coinTask = new CoinTask(coinString);

    private volatile PlotTask plotTask;

    public Dashboard() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Dashboard.
     */
    // TODO: Rename and change types and number of parameters
    public static Dashboard newInstance(String param1, String param2) {
        Dashboard fragment = new Dashboard();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Called when the fragment object is created.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        this.setRetainInstance(true);
    }

    /**
     * Called when the view attached to the fragment object is created.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        handler = new Handler();

        // Instantiating all of the TextView objects.
        priceView = view.findViewById(R.id.priceView);
        priceLblView = view.findViewById(R.id.priceLbl);
        supplyView = view.findViewById(R.id.supply);
        changeDayView = view.findViewById(R.id.changeDay);
        openDayView = view.findViewById(R.id.openDay);
        highDayView = view.findViewById(R.id.highDay);
        lowDayView = view.findViewById(R.id.lowDay);
        marketCapView = view.findViewById(R.id.marketCap);
        change24View = view.findViewById(R.id.change24);
        open24View = view.findViewById(R.id.open24);
        high24View = view.findViewById(R.id.high24);
        low24View = view.findViewById(R.id.low24);

        // Instantiating all of the Button objects
        btcBtn = view.findViewById(R.id.btcBtn);
        ethBtn = view.findViewById(R.id.ethBtn);
        ltcBtn = view.findViewById(R.id.ltcBtn);
        minChartBtn = view.findViewById(R.id.minChartBtn);
        hourChartBtn = view.findViewById(R.id.hourChartBtn);
        dayChartBtn = view.findViewById(R.id.dayChartBtn);

        // Plot instantiation.
        coinPlot = (XYPlot) view.findViewById(R.id.plot);
        // TODO: Figure out how to do this using XML and not Java.
        coinPlot.getGraph().setPaddingLeft(50);
        coinPlot.getGraph().setPaddingBottom(50);
        plotTitle = view.findViewById(R.id.plotTitle);
        plotTask = new PlotTask("BTC");

        // Setting the onClick listener to look for the fragment and not the MainActivity.
        View.OnClickListener btcBtnClick = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("CLICKY", "BTC BUTTON");
                stopCoinTask();
                stopPlotTask();
                if (networkStateReceiver.getIsConnected()) {
                    coinString = "BTC";
                    coinTask = new CoinTask(coinString);
                    plotTask = new PlotTask(coinString);
                    startCoinTask();
                    startPlotTask();
                }

            }
        };

        View.OnClickListener ethBtnClick = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("CLICKY2", "ETH BUTTON");
                stopCoinTask();
                stopPlotTask();
                if (networkStateReceiver.getIsConnected()) {
                    coinTask = new CoinTask("ETH");
                    plotTask = new PlotTask("ETH");
                    startCoinTask();
                    startPlotTask();
                }
            }
        };

        View.OnClickListener ltcBtnClick = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("CLICKY3", "LTC BUTTON");
                stopCoinTask();
                stopPlotTask();
                if (networkStateReceiver.getIsConnected()) {
                    coinTask = new CoinTask("LTC");
                    plotTask = new PlotTask("LTC");
                    startCoinTask();
                    startPlotTask();
                }
            }
        };

        View.OnClickListener minChartBtnClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlotTask();
                Log.i("CLICKY4", "MIN CHART BUTTON");
                minutePressed = true;
                hourPressed = false;
                dayPressed = false;
                startPlotTask();
            }
        };

        View.OnClickListener hourChartClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlotTask();
                Log.i("CLICKY5", "HOUR CHART BUTTON");
                minutePressed = false;
                hourPressed = true;
                dayPressed = false;
                startPlotTask();
            }
        };

        View.OnClickListener dayChartClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlotTask();
                Log.i("CLICKY6", "DAY CHART BUTTON");
                minutePressed = false;
                hourPressed = false;
                dayPressed = true;
                startPlotTask();
            }
        };

        // Sets the on click listener for the coin buttons.
        btcBtn.setOnClickListener(btcBtnClick);
        ethBtn.setOnClickListener(ethBtnClick);
        ltcBtn.setOnClickListener(ltcBtnClick);
        minChartBtn.setOnClickListener(minChartBtnClick);
        hourChartBtn.setOnClickListener(hourChartClick);
        dayChartBtn.setOnClickListener(dayChartClick);

        // Sets up the network receiver listener objects.
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        getActivity().registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This method is called when the fragment is attached.
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        //******CALLS ADDED BY MAX FOR TESTING**********//
        //CoinList cl = new CoinList();
//        Coin testCoin = new Coin("BTC");
//        testCoin.setChartMinute();
//        List<OHLCV> result = testCoin.getChartMinute();
//        for (int i = 0; i < result.size(); i++) {
//            Log.d("Next:\n", result.get(i).toString() + "\n");
//            System.out.println("result.get(i).toString()");
//        }
        //**********************************************//
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * This method is called when the fragment is detached.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        networkStateReceiver.removeListener(this);
        this.getActivity().unregisterReceiver(networkStateReceiver);
        this.stopCoinTask();
        mListener = null;
    }

    /**
     * This method is used to start the coin information grabbing task with a delay of 100ms
     */
    public void startCoinTask() {
        handler.postDelayed(coinTask, 100);
    }

    /**
     * This method is used to remove the coin task from the handler callbacks.
     */
    public void stopCoinTask() {
        handler.removeCallbacks(coinTask);
    }

    /**
     * This method starts the plotting of the coin information which is handled separately.
     * Default delay: 100ms
     */
    public void startPlotTask() {
        handler.postDelayed(plotTask, 100);
    }

    /**
     * This method is used to stop the plotting of the coin information by removing the plot task
     * from the handler callbacks.
     */
    public void stopPlotTask() {
        handler.removeCallbacks(plotTask);
    }

    /**
     * This method is called when a network available event is handled. Resume any processes here
     * that have to deal with network requests.
     */
    @Override
    public void networkAvailable() {
        Log.i("NETWORK", "AVAILABLE DASHBOARD");
        if (!networkAvailabilityAlreadyCalled) {
            networkAvailabilityAlreadyCalled = true;
            this.startCoinTask();
            this.startPlotTask();
        }
        // Display a toast message when the network is available.
        // Toast.makeText(getContext(), "Connection restored", Toast.LENGTH_SHORT).show();
    }

    /**
     * This method is called when a network unavailable event is handled. Stop any processes/threads
     * that have to deal with network request.
     */
    @Override
    public void networkUnavailable() {
        Log.i("NETWORK", "UNAVAILABLE DASHBOARD");

        // Sets the boolean flag to false so network available can be called.
        networkAvailabilityAlreadyCalled = false;
        this.stopCoinTask();
        this.stopPlotTask();
        priceView.setText("Loading...");
        supplyView.setText("Loading...");
        changeDayView.setText("Loading...");
        openDayView.setText("Loading...");
        highDayView.setText("Loading...");
        lowDayView.setText("Loading...");
        marketCapView.setText("Loading...");
        change24View.setText("Loading...");
        open24View.setText("Loading...");
        high24View.setText("Loading...");
        low24View.setText("Loading...");
        // Display a toast message when the network is unavailable.
        Toast.makeText(getContext(), "Please connect to the internet", Toast.LENGTH_SHORT).show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * This class is used internally with
     */
    private class CoinTask implements Runnable {

        private String coinString;

        public CoinTask(String coinString) {
            this.coinString = coinString;
        }

        @Override
        public void run() {

            // CHANGE THE DELAY UP ABOVE IN updateDashboardInterval DECLARATION.
            handler.postDelayed(this, updateDashboardInterval);

            Coin coin = new Coin(coinString);
            coin.setCurrInfo();

            String price = coin.getCurrInfo().getPrice();
            String priceLbl = coinString;
            String supply = coin.getCurrInfo().getSupply();
            String changeDay = coin.getCurrInfo().getChangeDay();
            String openDay = coin.getCurrInfo().getOpenDay();
            String highDay = coin.getCurrInfo().getHighDay();
            String lowDay = coin.getCurrInfo().getLowDay();
            String marketCap = coin.getCurrInfo().getMarketCap();
            String change24 = coin.getCurrInfo().getChange24();
            String open24 = coin.getCurrInfo().getOpen24();
            String high24 = coin.getCurrInfo().getHigh24();
            String low24 = coin.getCurrInfo().getLow24();

            if (debugPrintCoinPrice) {
                Log.i("BTC PRICE", price);
            }
            priceView.setText(price);
            priceLblView.setText(priceLbl + " Price:");
            supplyView.setText(supply);
            changeDayView.setText(changeDay);

            // Changes the color of the change
            if (coin.getCurrInfo().isPosDay() == true) {
                changeDayView.setTextColor(Color.GREEN);
            } else if (coin.getCurrInfo().isPosDay() == false) {
                changeDayView.setTextColor(Color.RED);
            } else {
                changeDayView.setTextColor(Color.parseColor("#F5F5F5"));
            }

            openDayView.setText(openDay);
            highDayView.setText(highDay);
            lowDayView.setText(lowDay);
            marketCapView.setText(marketCap);
            change24View.setText(change24);

            if (coin.getCurrInfo().isPos24() == true) {
                change24View.setTextColor(Color.GREEN);
            } else if (coin.getCurrInfo().isPos24() == false) {
                change24View.setTextColor(Color.RED);
            } else {
                change24View.setTextColor(Color.parseColor("#F5F5F5"));
            }
            open24View.setText(open24);
            high24View.setText(high24);
            low24View.setText(low24);
        }
    }

    private class PlotTask implements Runnable {
        private String coinString;

        public PlotTask(String coinString) {
            this.coinString = coinString;
        }

        @Override
        public void run() {
            handler.postDelayed(this, updatePlotInterval);

            // TODO: Don't constantly change the text for the plot title. This is a temp solution.
            plotTitle.setText(coinString + " Market");

            Coin coin = new Coin(coinString);

            List<OHLCV> coinPriceHistory;

            if (minutePressed) {
                // If minute chart button has been pressed then get the chart minute data from coin.
                coin.setChartMinute();
                coinPriceHistory = coin.getChartMinute();
            } else if (hourPressed) {
                // If hour chart button has been pressed then get the chart hour data from coin.
                coin.setChartHour();
                coinPriceHistory = coin.getChartHour();
            } else if (dayPressed) {
                // If day chart button has been pressed then get the chart day data from coin.
                coin.setChartDay();
                coinPriceHistory = coin.getChartDay();
            } else {
                coin.setChartMinute();
                coinPriceHistory = coin.getChartMinute();
            }

            Number[] coinPriceArray = new Number[coinPriceHistory.size()];
            final String[] dateArray = new String[coinPriceHistory.size()];

            for (int i = 0; i < coinPriceArray.length; i++) {
                coinPriceArray[i] = coinPriceHistory.get(i).getClose();
                dateArray[i] = splitTime(coinPriceHistory.get(i).getTimeAsString());
                Log.i("DATE LOOP", coinPriceHistory.get(i).getTimeAsString());
            }

            plotNumbers(dateArray, coinPriceArray, coinString);
        }
    }

    /**
     * This method takes in the time string given by the OHLCV object and parses the time stamp
     * excluding the data to just get the current time. This is used for the domain values in
     * the plot.
     * <p>
     * Ex: 2018-04-08 16:11:00 EDT -> 16:11:00
     *
     * @param timeString OHLCV date as string
     * @return The truncated time stamp.
     */
    private String splitTime(String timeString) {
        String retString;
        String[] splitText = timeString.split(" ");
        // TODO: Please make this more general. Such an asshole way of doing this. I made a short fix. - Love Logie
        if (dayPressed) {
            retString = splitText[0];
        } else {
            retString = splitText[1];
        }

        return retString;
    }

    /**
     * This method takes in array of domain labels, a series to plot, and a string for the key for
     * the line being plotted.
     * @param domainLabels
     * @param series
     * @param coinString
     */
    private void plotNumbers(final String[] domainLabels, Number[] series, String coinString) {

        coinPlot.clear();
        // turn the above arrays into XYSeries':
        // (Y_VALS_ONLY means use the element index as the x value)
        XYSeries series1 = new SimpleXYSeries(
                Arrays.asList(series), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, coinString + " Price");

        // create formatters to use for drawing a series using LineAndPointRenderer
        // and configure them from xml:
        LineAndPointFormatter series1Format =
                new LineAndPointFormatter(getActivity(), R.xml.line_point_formatter_with_labels);


        // just for fun, add some smoothing to the lines:
        // see: http://androidplot.com/smooth-curves-and-androidplot/
        series1Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(10, CatmullRomInterpolator.Type.Centripetal));


        // add a new series' to the xyplot:
        coinPlot.addSeries(series1, series1Format);

        coinPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                int i = Math.round(((Number) obj).floatValue());
                return toAppendTo.append(domainLabels[i]);
            }

            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });

        coinPlot.redraw();
    }
}