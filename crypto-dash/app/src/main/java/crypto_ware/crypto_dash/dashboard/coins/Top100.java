package crypto_ware.crypto_dash.dashboard.coins;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 3/14/18.
 */

public class Top100 {
    //List of the top 100 currencies by price
    private List<Coin> top100;
    //String of url for top 100 list
    private static String top100URL = "https://min-api.cryptocompare.com/data/top/volumes?tsym=USD&limit=100";
    //Flag for wait on request completion
    private volatile boolean reqComp;

    public Top100() {
        reqComp = false;
        new GetTop100().execute();
        while (!reqComp) {
            //wait while request goes through
        }
    }

    private class GetTop100 extends AsyncTask<Void, Void, Void> {
        //Tag for any messages related to class
        private static final String TAG = "GetTop100";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //Initialize RequestHandle object
            RequestHandle rhCL = new RequestHandle();
            //Make a request based on given url
            String top100Raw = rhCL.execRequest(top100URL);

            if (top100Raw != null) {
                //Initialize coin list object;
                top100 = rhCL.parseTop100(top100Raw);
                reqComp = true;
            }
            return null;
        }

        /**
         * GUI updates go here
         * @param aVoid
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    //THIS IS A CRUDE SEARCH FOR TESTING, NEEDS TO BE UPDATED
    public Coin getCoin(String name) {
        for (int i = 0; i < top100.size(); i ++) {
            Coin nxt = top100.get(i);
            if (name.equalsIgnoreCase(nxt.getName())) {
                return nxt;
            }
        }
        return null;
    }

    public List<Coin> getButtonPanel() {
        List<Coin> buttonCoins = new ArrayList<>();
        for(int i = 0; i < 3; i++) {
            buttonCoins.add(top100.get(i));
        }
        return buttonCoins;
    }
}
