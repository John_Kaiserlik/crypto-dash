package crypto_ware.crypto_dash.dashboard.coins;

import android.os.AsyncTask;

import java.util.List;

/**
 * Coin Object will contain all relevant identification info for a given coin.
 * In addition all current chart data(List<OHLCV> Objects) and price information(PriceInfo Object)
 * will also be stored in each instance of the Coin class when requested.
 */
public class Coin {
    //ID number of the coin
    private int id;
    //Name = coin "initials", coinName = coin's full name
    private String name, coinName;
    //Array of OHLCV objects for each chart interval type
    private volatile List<OHLCV> chartMinute, chartHour, chartDay;
    //Identifiers for the type of request sent
    private static final int CHART_MIN = 0;
    private static final int CHART_HOUR = 1;
    private static final int CHART_DAY = 2;
    private static final int PRICE_INFO = 3;
    //Used in switch statement within ASyncTask(GetCurrData) for request type identification
    private int reqType;
    //PriceInfo object storing the most up to date price info
    private volatile PriceInfo currInfo;
    //URL bases for supported request types
    private String[] baseURLS = {"https://min-api.cryptocompare.com/data/histominute?",
            "https://min-api.cryptocompare.com/data/histohour?",
            "https://min-api.cryptocompare.com/data/histoday?",
            "https://min-api.cryptocompare.com/data/pricemultifull?"};
    //Flag for wait on request completion
    private volatile boolean taskComplete = false;
    private AsyncTask asyncMin, asyncHR, asyncDay, asyncPI;

    //Default blank constructor
    public Coin() {

    }

    //Constructor requiring only name(3, or 4 letter initials)
    public Coin(String name) {
        this.name = name;
    }

    //Setter methods
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCoinName(String cName) {
        this.coinName = cName;
    }

    //Simple get methods
    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getCoinName() {
        return this.coinName;
    }


    //Setter and Getter for minute interval chart
    public void setChartMinute() {
        taskComplete = false;
        this.reqType = CHART_MIN;
        asyncMin = new GetCurrData().execute();
    }

    public List<OHLCV> getChartMinute() {
        while (!taskComplete) {
            //wait for task completion
        }
        List<OHLCV> result = chartMinute;
        for (int i = 0; i < result.size(); i++) {
            String teststring = result.get(i).toString() + "\n";
            System.out.println("Next:\n" + teststring);
        }
        return chartMinute;
    }

    //Setter and Getter for hour interval chart
    public void setChartHour() {
        taskComplete = false;
        this.reqType = CHART_HOUR;
        asyncHR = new GetCurrData().execute();
    }

    public List<OHLCV> getChartHour() {
        while (!taskComplete) {
            //wait for task completion
        }
        return this.chartHour;
    }

    //Setter and Getter for day interval chart
    public void setChartDay() {
        taskComplete = false;
        this.reqType = CHART_DAY;
        asyncDay = new GetCurrData().execute();
    }

    public List<OHLCV> getChartDay() {
        while (!taskComplete) {
            //wait for task completion
        }
        return this.chartDay;
    }

    //Setter and Getter for current price information
    public void setCurrInfo() {
        taskComplete = false;
        this.reqType = PRICE_INFO;
        asyncPI = new GetCurrData().execute();
    }

    public PriceInfo getCurrInfo() {
        while (!taskComplete) {
            // Log.i("TASK", "NOT COMPLETE");
        }
        return this.currInfo;
    }

    //Called to cancel AsyncTask for chart minute data
    public void cancelChartMin() {
        asyncMin.cancel(true);
    }

    //Called to cancel AsyncTask for chart hour data
    public void cancelChartHour() {
        asyncHR.cancel(true);
    }

    //Called to cancel AsyncTask for chart day data
    public void cancelChartDay() {
        asyncDay.cancel(true);
    }

    //Called to cancel AsyncTask for price info
    public void cancelPriceInfo() {
        asyncPI.cancel(true);
    }

    //Simple to string method for basic identification information of coin.
    public String toString() {

        return "Coin Info: [\n" + "ID: " + this.id + "\n" + "Name: " + this.name + "\n"
                + "Coin Name: " + this.coinName + "\n]\n";
    }

    /**
     * This class sends out any network requests to be handled by various methods of the RequestHandle
     * class. Switch statement determines the request type. Upon completion the field corresponding to
     * the request type is populated.
     * (For Logan) Corresponding GUI updates will likely be added into onPostExecute() of this class.
     */
    private class GetCurrData extends AsyncTask<Void, Void, Void> {
        private static final String TAG = "GetCurrData";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (!isCancelled()) {
                //url = URL to be constructed, rawChart = raw string of returned chart data, rawInfo = raw string of returned price info.
                String url, rawChart, rawInfo;

                //Handles the sending/parsing of all outgoing requests.
                RequestHandle chartRH = new RequestHandle();

                switch (reqType) {
                    //Minute interval OHLCV's for roughly one week
                    case CHART_MIN:
                        url = baseURLS[CHART_MIN] + "fsym=" + getName() + "&tsym=USD&limit=9";
                        rawChart = chartRH.execRequest(url);
                        chartMinute = chartRH.parseOHLCV(rawChart);
                        taskComplete = true;
                        break;
                    //Hour interval OHLCV's for roughly one month
                    case CHART_HOUR:
                        url = baseURLS[CHART_HOUR] + "fsym=" + getName() + "&tsym=USD&limit=9";
                        rawChart = chartRH.execRequest(url);
                        chartHour = chartRH.parseOHLCV(rawChart);
                        taskComplete = true;
                        break;
                    //Day interval OHLCV's for entire stored history
                    case CHART_DAY:
                        url = baseURLS[CHART_DAY] + "fsym=" + getName() + "&tsym=USD&limit=9";
                        rawChart = chartRH.execRequest(url);
                        chartDay = chartRH.parseOHLCV(rawChart);
                        taskComplete = true;
                        break;
                    //Current Price Info for coin
                    case PRICE_INFO:
                        url = baseURLS[PRICE_INFO] + "fsyms=" + getName() + "&tsyms=USD";
                        rawInfo = chartRH.execRequest(url);
                        currInfo = chartRH.parseCurrInfo(rawInfo, getName());
                        taskComplete = true;
                        break;
                    default:
                        return null;
                }
            }
            return null;

        }

        /**
         * GUI updates go here
         *
         * @param aVoid
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }
}
