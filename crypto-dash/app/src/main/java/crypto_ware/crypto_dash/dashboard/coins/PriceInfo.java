package crypto_ware.crypto_dash.dashboard.coins;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * PriceInfo will contain the most updated information for a given Coin Object.
 * Created by max on 3/6/18.
 */

public class PriceInfo {

    private Date time;
    private String price;
    private String openDay, highDay, lowDay, changeDay;
    private String open24, high24, low24, change24, tVol24;
    private String supply, marketCap;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    private Boolean isPosDay, isPos24;

    /**
     * Blank Constructor
     */
    public PriceInfo() {
        this.time = new Date();
        this.price = "$0.00";
        this.openDay = "";
        this.highDay = "";
        this.lowDay = "";
        this.changeDay = "";
        this.open24 = "";
        this.high24 = "";
        this.low24 = "";
        this.change24 = "";
        this.tVol24 = "";
        this.supply = "";
        this.marketCap = "";
        this.isPosDay = false;
        this.isPos24 = false;
    }

    /**
     * Full Constructor
     *
     * @param t    time
     * @param p    price
     * @param oD   open day
     * @param hD   high day
     * @param lD   low day
     * @param cD   change day
     * @param o24  open 24 hour
     * @param h24  high 24 hour
     * @param l24  low 24 hour
     * @param c24  change 24 hour
     * @param tV24 total volume 24 hour
     * @param supp supply
     * @param mC   market cap
     */
    public PriceInfo(Date t, String p, String oD, String hD, String lD, String cD,
                     String o24, String h24, String l24, String c24, String tV24,
                     String supp, String mC) {
        this.time = t;
        this.price = p;
        this.openDay = oD;
        this.highDay = hD;
        this.lowDay = lD;
        this.changeDay = cD;
        this.open24 = o24;
        this.high24 = h24;
        this.low24 = l24;
        this.change24 = c24;
        this.tVol24 = tV24;
        this.supply = supp;
        this.marketCap = mC;

    }

    //Setter Methods
    public void setTime(Date t) {
        this.time = t;
    }

    public void setPrice(String p) {
        this.price = p;
    }

    public void setOpenDay(String oD) {
        this.openDay = oD;
    }

    public void setHighDay(String hD) {
        this.highDay = hD;
    }

    public void setLowDay(String lD) {
        this.lowDay = lD;
    }

    public void setChangeDay(String cD) {
        this.changeDay = cD;
    }

    public void setOpen24(String o24) {
        this.open24 = o24;
    }

    public void setHigh24(String h24) {
        this.high24 = h24;
    }

    public void setLow24(String l24) {
        this.low24 = l24;
    }

    public void setChange24(String c24) {
        this.change24 = c24;
    }

    public void settVol24(String tV24) {
        this.tVol24 = tV24;
    }

    public void setSupply(String supp) {
        this.supply = supp;
    }

    public void setMarketCap(String mC) {
        this.marketCap = mC;
    }

    public void setPosDay(Boolean isPos) {
        this.isPosDay = isPos;
    }

    public void setPos24(Boolean isPos) {
        this.isPos24 = isPos;
    }


    //Getter Methods
    public Date getTime() {
        return time;
    }

    public String getPrice() {
        return price;
    }

    public String getOpenDay() {
        return openDay;
    }

    public String getHighDay() {
        return highDay;
    }

    public String getLowDay() {
        return lowDay;
    }

    public String getChangeDay() {
        return changeDay;
    }

    public String getOpen24() {
        return open24;
    }

    public String getHigh24() {
        return high24;
    }

    public String getLow24() {
        return low24;
    }

    public String getChange24() {
        return change24;
    }

    public String gettVol24() {
        return tVol24;
    }

    public String getSupply() {
        return supply;
    }

    public String getMarketCap() {
        return marketCap;
    }

    public Boolean isPosDay() {
        return isPosDay;
    }

    public Boolean isPos24() {
        return isPos24;
    }

    /**
     * Returns Date field stored in PriceInfo Object in readable format based on machine's current time zone
     *
     * @return, formatted date as String
     */
    public String getTimeAsString() {
        DATE_FORMAT.setTimeZone(TimeZone.getDefault());
        return DATE_FORMAT.format(this.time);
    }

}
