package crypto_ware.crypto_dash.dashboard.coins;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * RequestHandle Object will send out all network requests for the Dashboard system.
 * After returning raw results, the RequestHandle class also provides methods for parsing
 * each request type supported by this system.
 * Created by max on 2/7/18.
 */

public class RequestHandle {

    //Tag to label any potential error messages
    private static final String TAG = "RequestHandle";
    //All request types are get
    private static final String REQ_METHOD = "GET";
    //Need to add fields for time limits on requests

    /**
     * Blank Constructor
     */
    public RequestHandle() {
    }

    /**
     * Handles sending a generic get request, reads data in then converts raw results into a string
     *
     * @param reqURL, the url for a given request.
     * @return
     */
    public String execRequest(String reqURL) {
        //For testing purposes
        Log.d("url being sent: ", reqURL);
        //String to hold a response from outside server
        String response = null;
        //URL Object to hold request url string
        URL url;

        try {
            //Initialize URL
            url = new URL(reqURL);

            try {
                //Establish connection and set request type
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod(REQ_METHOD);

                try {
                    //Read in results
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    //Build String from results
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    //return results as raw string
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
                return null;
            } catch (Exception ex) {
                Log.i("CAUGHT", "REQUEST HANDLE execRequest UNKNOWN EXCEPTION (INNER TRY-CATCH)");
                return null;
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        } catch (Exception ex) {
            Log.i("CAUGHT", "REQUEST HANDLE execRequest UNKNOWN EXCEPTION (OUTER TRY-CATCH)");
            return null;
        }
    }

    /**
     * Parses request for full coin list, builds List<Coin> Object then returns to sender.
     *
     * @param rawList, raw results from request to be parsed
     * @return
     */
    public List<Coin> parseCoinList(String rawList) {
        List<Coin> cL = new ArrayList<>();
        try {
            //Convert received string to JSON Object
            JSONObject coinListObject = new JSONObject(rawList);
            //Grab Data Object from within contents
            JSONObject coinData = coinListObject.getJSONObject("Data");
            //Place coin names in an iterator for scanning list
            Iterator coinNames = coinData.keys();

            //Iterate through coin names, build coin objects
            while (coinNames.hasNext()) {

                String nxtCoin = (String) coinNames.next();
                JSONObject coin = coinData.getJSONObject(nxtCoin);
                Coin next = new Coin(coin.getString("Name"));
                next.setId(coin.getInt("Id"));
                next.setCoinName(coin.getString("CoinName"));

                cL.add(next);
                Log.d("Next Coin: ", next.toString());
            }

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (Exception ex) {
            Log.i("CAUGHT", "REQUEST HANDLE parseCoinList UNKNOWN EXCEPTION");
        }
        return cL;
    }

    /**
     * Parses request results for chart data, Deals with all three interval types generically,
     * returns a List<OHLCV> object to sender.
     *
     * @param rawChart
     * @return
     */
    public List<OHLCV> parseOHLCV(String rawChart) {
        List<OHLCV> chartData = new ArrayList<>();
        try {
            JSONObject fullData = new JSONObject(rawChart);
            JSONArray data = fullData.getJSONArray("Data");

            for (int i = 0; i < data.length(); i++) {
                //Next OHLCV Object in array
                OHLCV nxtOHLCV = new OHLCV();
                //Next JSON Object to be parsed
                JSONObject nxtJSON = data.getJSONObject(i);
                //UNIX timestamp
                long time = nxtJSON.getLong("time");
                //Convert to miliseconds, Create Date Object
                Date timeStamp = new Date(time * 1000L);
                //Add to OHLCV
                nxtOHLCV.setTime(timeStamp);
                //Add close price
                nxtOHLCV.setClose(nxtJSON.getDouble("close"));
                //Add high price
                nxtOHLCV.setHigh(nxtJSON.getDouble("high"));
                //Add low price
                nxtOHLCV.setLow(nxtJSON.getDouble("low"));
                //Add open price
                nxtOHLCV.setOpen(nxtJSON.getDouble("open"));
                //Add volume leaving from the currency
                nxtOHLCV.setVolFrom(nxtJSON.getDouble("volumefrom"));
                //Add volume entering into the currency
                nxtOHLCV.setVolTo(nxtJSON.getDouble("volumeto"));
                //Add to ArrayList
                chartData.add(nxtOHLCV);
            }
            return chartData;
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (Exception ex) {
            Log.i("CAUGHT", "REQUEST HANDLE parseOHLCV UNKNOWN EXCEPTION");
        }
        return chartData;
    }

    /**
     * @param rawInfo
     * @return
     */
    public PriceInfo parseCurrInfo(String rawInfo, String coinName) {
        PriceInfo currInfo = new PriceInfo();
        try {
            // TODO: Establish dynamic naming for other coins in the future.
            JSONObject fullData = new JSONObject(rawInfo);
            JSONObject rawValues = fullData.getJSONObject("RAW");
            JSONObject fromSym = rawValues.getJSONObject(coinName);
            JSONObject toSym = fromSym.getJSONObject("USD");
            JSONObject displayValues = fullData.getJSONObject("DISPLAY");
            JSONObject fromSymDisplay = displayValues.getJSONObject(coinName);
            JSONObject toSymDisplay = fromSymDisplay.getJSONObject("USD");

            //Check if day and 24 hour changes in price are positive or negative(For GUI display color)

            double changeDay = toSym.getDouble("CHANGEDAY");
            //Display value as green
            if (changeDay > 0) {
                currInfo.setPosDay(true);
            }
            //Display value as red
            else if (changeDay < 0) {
                currInfo.setPosDay(false);
            }
            //Display value as neutral color
            else {
                currInfo.setPosDay(null);
            }
            double change24 = toSym.getDouble("CHANGE24HOUR");
            //Display value as green
            if (change24 > 0) {
                currInfo.setPos24(true);
            }
            //Display value as red
            else if (change24 < 0) {
                currInfo.setPos24(false);
            }
            //Display value as neutral color
            else {
                currInfo.setPos24(null);
            }

            //Populate PriceInfo Object fields
            //UNIX timestamp
            long time = toSym.getLong("LASTUPDATE");
            //Convert to miliseconds, Create Date Object
            Date timeStamp = new Date(time * 1000L);
            //Add to PriceInfo Object
            currInfo.setTime(timeStamp);
            //Set Price
            currInfo.setPrice(toSymDisplay.getString("PRICE"));
            //Set open price for day
            currInfo.setOpenDay(toSymDisplay.getString("OPENDAY"));
            //Set high price for day
            currInfo.setHighDay(toSymDisplay.getString("HIGHDAY"));
            //Set low price for day
            currInfo.setLowDay(toSymDisplay.getString("LOWDAY"));
            //Set change in price per day
            currInfo.setChangeDay(toSymDisplay.getString("CHANGEDAY"));
            //Set open price for 24 hour period
            currInfo.setOpen24(toSymDisplay.getString("OPEN24HOUR"));
            //Set high price for 24 hour period
            currInfo.setHigh24(toSymDisplay.getString("HIGH24HOUR"));
            //Set low price for 24 hour period
            currInfo.setLow24(toSymDisplay.getString("LOW24HOUR"));
            //Set change in price per 24 hour period
            currInfo.setChange24(toSymDisplay.getString("CHANGE24HOUR"));
            //Set total 24 hour volume
            currInfo.settVol24(toSymDisplay.getString("TOTALVOLUME24H"));
            //Set supply value
            currInfo.setSupply(toSymDisplay.getString("SUPPLY"));
            //Set market cap value
            currInfo.setMarketCap(toSymDisplay.getString("MKTCAP"));

            return currInfo;
        } catch (JSONException e) {
            Log.i("CAUGHT", "REQUEST HANDLE JSON EXCEPTION");
            Log.e(TAG, e.getMessage(), e);
        } catch (Exception e) {
            // Log.e(TAG,e.getMessage(), e);
            Log.i("CAUGHT", "REQUEST HANDLE UNKNOWN EXCEPTION");
        }
        return currInfo;
    }

    public List<Coin> parseTop100(String raw100) {
        List<Coin> top100 = new ArrayList<>();

        try {
            //Convert received string to JSON Object
            JSONObject top100JSON = new JSONObject(raw100);
            JSONArray top100JSONArray = top100JSON.getJSONArray("Data");
            //Iterate through coin names, build coin objects
            for (int i = 0; i < top100JSONArray.length(); i++) {
                Coin next = new Coin();
                JSONObject nxtJSON = top100JSONArray.getJSONObject(i);
                next.setName(nxtJSON.getString("SYMBOL"));
                next.setCoinName(nxtJSON.getString("NAME"));
                next.setId(nxtJSON.getInt("ID"));
                top100.add(next);
            }

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (Exception ex) {
            Log.i("CAUGHT", "REQUEST HANDLE PARSE TOP 100 UNKNOWN EXCEPTION");
        }
        return top100;
    }
}
