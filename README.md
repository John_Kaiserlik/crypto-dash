# Crypto-Dash #

Welcome to the private Crypto-Dash BitBucket repository for the Android version of the crypo-currency market monitoring tool.


(What a mouthful!)
![The currency of the internet](https://vladimirribakov.com/wp-content/uploads/Crypt.jpg)

### What is this application for? ###

This is an Android application that attempts to consolidate various crypto-currencies into one location. That way the user can view the real-time market statistics and decide which crypto-currencies to invest in or to steer away from.
Additionally, Crypto-Dash provides the latest crypto-currency news just by swiping to the left and some handy tutorial by swiping to the right. Simple right?

### Where are we now? ###
As of now, Crypto-Dash is able to display real-time market statistics, give an update on crypto-currency news, and provide tutorials on the 
concepts of crypto-currencies. The top three coins Bitcoin, Etherium, and Litecoin are supported. In future releases up to 2000+ coins will
be supported. Coming soon!

![News Feed](docs/demo/app_demo_news.gif)
![Dashboard](docs/demo/app_demo_dashboard.gif)
![Tutorials Section](docs/demo/app_demo_tutorials.gif)

### Real-time Market Statistics ###
Below, we fast-forwarded the app to show the plotting of the market data for Bitcoin.
![Graphing](docs/demo/app_demo_graph.gif =300)


### How do I get set up? ###

* Clone the repository in a directory of your choice.
* Load the project in Android Studio (don't open a new one).
* Build and run the project in a simulator or on an actual Android device.
  - Note that at this time the Android version has not been chosen yet. Most likely this project will not support versions older than Lollipop.

### Contribution guidelines ###

* __Avoid commiting to the master branch. Doing so without fetching or pulling the changes could result in an old version replacing the newest version in which it would be lost.__
* Follow the general agreed upon programming style guide.
* Never commit broken or unfinished code.
* If you submit any code that does not follow the guidelines it will be removed or denied (if it is a pull request).
* All merges with the master branch must be done by a pull request.
* In terms of git management there is no leader so all pull requests should be allowed if everyone mutually agrees.

### Group communication ###

* The main group discussion will be held in Slack -> cryptocapstone.slack.com
* Or feel free to email the other contributors to the project:
- Logan Stanfield: lgstanfi@uncg.edu 
- Thomas Pedraza: ltpedraz@uncg.edu
- Max Bushman: mkbushma@uncg.edu
- John Kaiserlik: jrkaise2@uncg.edu

### Additional Resources ###
* [Git Cheat Sheet](https://www.git-tower.com/blog/git-cheat-sheet/)
* [Google Android Documentation](https://developer.android.com/studio/intro/index.html)
* [Design Patterns](https://en.wikipedia.org/wiki/Design_Patterns)
* [How to buy cryptocurrencies](https://getcrypto.info/)

### API's That Power Crypto-Dash ###
* [CryptoCompare](https://github.com/Josh-McFarlin/CryptoCompareAPI)
* [News API](https://newsapi.org/)
* [Android Plot](http://androidplot.com/)
* [Page Indicator](https://github.com/romandanylyk/PageIndicatorView)
